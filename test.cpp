#include <iostream>
#include <opencv2/core/utility.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>

using namespace std;
using namespace cv;

Mat image, gray, edge;

int main (void) {




	string filename="fruits.jpg";

	cout << "Loading image '" << filename << "'" << endl;

	image=imread(filename, 1);
	if(image.empty()) {

		cout << "error reading image" << endl;
		return 0;
	}

	cout << "Image read" << endl;

	cvtColor(image, gray, COLOR_BGR2GRAY);
	blur(gray, edge, Size(3,3));
	Canny(edge, edge, 1, 1*3, 3);

	vector<int> params;
	imwrite("out.jpg", edge, params);

	return 0;
}