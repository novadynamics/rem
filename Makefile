GEN_SRC := 
GEN_OBJ := $(patsubst %.cpp,%.o, $(GEN_SRC))

INC := -I/usr/local/

all: test 

test: test.o  $(GEN_OBJ)
	$(CXX) $^ -o $@ -L/usr/local/lib -lopencv_core -lopencv_imgproc -lopencv_imgcodecs


%.o: %.cpp 
	$(CXX) -msse2 -Wall $(INC) -c $< -o $@

clean:
	$(RM) *.o 
